import {createRouter, createWebHistory} from 'vue-router'  

import Boxes from "@/views/Boxes.vue";
import BindingData from "@/views/BindingData.vue";
import Cards from "@/views/Cards.vue";
import PaginaNaoEncontrada from "@/views/PaginaNaoEncontrada.vue";

const routes = [
  {
    path: "/",
    name: "Boxes",
    component: Boxes,
  },
  {
    path: "/binding-data",
    name: "BindingData",
    component: BindingData,
  },
  {
    path: "/data-card",
    name: "Cards",
    component: Cards,
  },

  {
    path: "/:pathMatch(.*)*",
    name: "Página não encontrada",
    component: PaginaNaoEncontrada,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
